<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use App\TodoList;

class TasksController extends Controller
{
    /**
     * Display a list of the tasks for the list corresponding with $todoListId.
     *
     * @param  int      $todoListId
     * @return Response
     */
    public function index($todoListId)
    {
        return Task::where('todo_list_id', $todoListId)->get();
    }

    /**
     * Store a newly created task for the list corresponding with $todoListId.
     *
     * @param  int      $todoListId
     * @return Response
     */
    public function store($todoListId)
    {
        $list = TodoList::find($todoListId);

        $task = new Task;
        $task->name = Input::get('name');
        $task->todoList()->associate($list);

        $task->save();

        return Task::find($task->id);
    }

    /**
     * Display the specified task.
     *
     * @param  int      $todoListId
     * @param  int      $taskId
     * @return Response
     */
    public function show($todoListId, $taskId)
    {
        return Task::where('todo_list_id', $todoListId)->findOrFail($taskId);
    }

    /**
     * Update the specified task.
     *
     * @param  int  $todoListId
     * @param  int  $taskId
     * @return int
     */
    public function update($todoListId, $taskId)
    {
        return Task::where('todo_list_id', $todoListId)
            ->where('id', $taskId)->update(Input::all());
    }

    /**
     * Remove the specified task from the specified list.
     *
     * @param  int  $todoListId
     * @param  int  $taskId
     * @return int
     */
    public function destroy($todoListId, $taskId)
    {
        return Task::where('todo_list_id', $todoListId)
            ->where('id', $taskId)
            ->delete();
    }

    /**
     * Display the specified task.
     *
     * @param  int      $todoListId
     * @param  int      $taskId
     * @param  bool     $taskId
     * @return Response
     */
    public function complete($todoListId, $taskId, $state = true)
    {
        Task::where('todo_list_id', $todoListId)
                    ->find($taskId)->update(['completed' => $state]);

        return Task::where('todo_list_id', $todoListId)->find($taskId);
    }
}
