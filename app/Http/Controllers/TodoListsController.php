<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TodoList;

class TodoListsController extends Controller
{
    /**
     * Display all the lists.
     *
     * @return Response
     */
    public function index()
    {
        return TodoList::with('tasks')->get();
    }

    /**
     * Store a new list.
     *
     * @return Response
     */
    public function store()
    {
        return TodoList::create(Input::all());
    }

    /**
     * Display the specified list.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return TodoList::with('tasks')->findOrFail($id);
    }

    /**
     * Update the specified list.
     *
     * @param  Illuminate\Http\Request $request
     * @param  int                     $id
     * @return int
     */
    public function update(Request $request, $id)
    {
        return TodoList::where('id', $id)->update(Input::all());
    }

    /**
     * Remove the specified list.
     *
     * @param  int  $id
     * @return int
     */
    public function destroy($id)
    {
        return TodoList::where('id', $id)->delete();
    }
}
