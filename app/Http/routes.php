<?php

Route::resource('lists', 'TodoListsController', ['except' => ['create', 'edit']]);

Route::resource('lists.tasks', 'TasksController', ['except' => ['create', 'edit']]);
Route::put('lists/{lists}/tasks/{tasks}/complete/{complete?}', 'TasksController@complete')
    ->where('complete', '0|1');
