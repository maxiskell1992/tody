<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'completed'];

    /**
     * A taks belongs to only one list.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function todoList()
    {
        return $this->belongsTo('App\TodoList');
    }
}
