<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    /**
     * The attributes that are amss assignable.
     */
    protected $fillable = ['name'];

    /**
     * A list can have many tasks.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
