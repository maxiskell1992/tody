<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\TodoList', 40)->create();

        factory('App\Task', 85)->create()->each(function($task) {
            $list = App\TodoList::all()->random();
            $task->todoList()->associate($list);
        });
    }
}
