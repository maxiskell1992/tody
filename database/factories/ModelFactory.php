<?php

$factory->define(App\TodoList::class, function ($faker) {
    return [
        'name' => $faker->bs
    ];
});

$factory->define(App\Task::class, function ($faker) {
    return [
        'name' => $faker->sentence,
        'todo_list_id' => App\TodoList::all()->random()->id
    ];
});
