# Tody

Ejemplo simple de la implementación de la API para un sistema de _todo-lists_.

El objetivo principal es demostrar mis habilidades en la utilización de un framework MVC en PHP (en éste caso, Laravel) para la implementación de una API RESTful, mi claridad y calidad de código.

Para mantener la simpleza y claridad, evité ciertas características y patrones de diseño:

- Utilización de repositorios e interfaces para un mayor nivel de abstracción del acceso a los datos
- Vistas / UI para el manejo de las listas; la aplicación responde con JSON
- Formato correcto de las respuestas, según la especificación encontrada en [JSON API](http://jsonapi.org)
- Utilización de una base de datos relacional, como MySQL. Para un rápido desarrollo y mantenimiento, la implementación trabaja con SQLite - lo cual es completamente configurable -
- Optimización de validaciones, _requests_ y _responses_ personalizadas
- Sistema de usuarios y autenticación para la creación de listas / tareas
- TDD
*-Manejo extensivo de excepciones

## Instalación

Para instalar la aplicación, siplemente clone el repositorio en el directorio deseado y corra las migraciones y los seeders, de la siguiente manera:

```
$ git clone https://maxiskell@bitbucket.org/maxiskell/tody.git tody
$ cd tody
$ php artisan migrate --seed
```

## Rutas

_Tomemos como referencia la url base tody.dev/_

### GET tody.dev/lists

Devuelve todas las lístas.

### POST tody.dev/lists

Crea una lista y devuelve la misma.

Parámetros:

- name: Nuevo nombre de la lista

### GET tody.dev/lists/{id}

Devuelve la lísta correspondiente al id dado.

### PUT tody.dev/lists/{id}

Modifica la lista correspondiente con el id dado.

Parámetros:

- name: nuevo nombre de la lista

### DELETE tody.dev/lists/{id}

Elimina la lísta correspondiente al id dado.

### GET tody.dev/lists/{id}/tasks

Devuelve todas las tareas correspondientes a lísta correspondiente al id dado.

### POST tody.dev/lists/{id}/tasks

Crea una nueva tarea para la lista correspondiente al id especificado.

Parámetros:

- name: nombre de la tarea

### GET tody.dev/lists/{listId}/tasks/{taskId}

Devuelve la tarea correspondiente al id de lista y tarea dados.

### PUT tody.dev/lists/{listId}/tasks/{taskId}

Modifica la tarea específicada para la lista correspondiente.

Parámetros:

- name: nuevo nombre de la tarea

### DELETE tody.dev/lists/{listId}/tasks/{taskId}

Elimina la tarea específicada para la lista correspondiente.

### PUT tody.dev/lists/{listId}/tasks/{taskId}/complete/{completed?}

Modifica el estado de una tarea entre completada y no completada.

Parámetros:

- completed: estado de completutyd de la tarea. El parámetro es opcional, por lo que si no se proporciona, por defecto se tomará como _true_, lo cual asignará a la tarea el estado de _'completa'_. Puede tomar sólo dos valores, 0 ó 1, para marcar la tarea como incompleta o completa respectivamente.